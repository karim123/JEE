package entity;

import entity.Equipement;
import entity.Research;
import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-25T17:31:44")
@StaticMetamodel(Utilisateur.class)
public class Utilisateur_ { 

    public static volatile SingularAttribute<Utilisateur, Equipement> equipementDirector;
    public static volatile SingularAttribute<Utilisateur, String> motDePasse;
    public static volatile SingularAttribute<Utilisateur, Timestamp> dateInscription;
    public static volatile ListAttribute<Utilisateur, Research> searchedList;
    public static volatile SingularAttribute<Utilisateur, String> phonenumber;
    public static volatile SingularAttribute<Utilisateur, Boolean> admin;
    public static volatile SingularAttribute<Utilisateur, Long> id;
    public static volatile SingularAttribute<Utilisateur, String> nom;
    public static volatile SingularAttribute<Utilisateur, String> email;

}