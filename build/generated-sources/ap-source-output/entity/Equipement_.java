package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-25T17:31:44")
@StaticMetamodel(Equipement.class)
public class Equipement_ { 

    public static volatile SingularAttribute<Equipement, String> depLib;
    public static volatile SingularAttribute<Equipement, String> institution;
    public static volatile SingularAttribute<Equipement, String> equipement;
    public static volatile SingularAttribute<Equipement, String> equipementNom;
    public static volatile SingularAttribute<Equipement, String> nature;
    public static volatile SingularAttribute<Equipement, String> commune;
    public static volatile SingularAttribute<Equipement, String> anne;
    public static volatile SingularAttribute<Equipement, Long> id;
    public static volatile SingularAttribute<Equipement, String> position;
    public static volatile SingularAttribute<Equipement, String> handi;
    public static volatile SingularAttribute<Equipement, String> depCode;

}