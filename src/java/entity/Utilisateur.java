package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Utilisateur implements Serializable{

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long      id;
    @Column(unique=true)
    @NotNull( message = "Veuillez saisir une adresse email" )
    @Pattern( regexp = "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)", message = "Merci de saisir une adresse mail valide" )
    private String    email;
    @Column( name = "mot_de_passe" )
    @NotNull( message = "Veuillez saisir un mot de passe" )
    @Size( min = 3, message = "Le mot de passe doit contenir au moins 3 caractères" )
    private String    motDePasse;
    @NotNull( message = "Veuillez saisir un nom d'utilisateur" )
    @Size( min = 3, message = "Le nom d'utilisateur doit contenir au moins 3 caractères" )
    private String    nom;
    @Column( name = "date_inscription" )
    private String phonenumber;

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }
    private Timestamp dateInscription;
    @OneToMany( cascade=CascadeType.PERSIST)
    private List<Research> searchedList;
    @OneToOne( cascade=CascadeType.PERSIST)
    private Equipement equipementDirector;

    public Equipement getEquipementDirector() {
        return equipementDirector;
    }

    public void setEquipementDirector(Equipement equipementDirector) {
        this.equipementDirector = equipementDirector;
    }
    private boolean admin;

    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Utilisateur(){
        searchedList = new ArrayList<>();
    }

    public List<Research> getSearchedList() {
        return searchedList;
    }

    public void setSearchedList(List<Research> searchedList) {
        this.searchedList = searchedList;
    }
    
    public void addSearch(Research research){
        searchedList.add(research);
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Timestamp getDateInscription() {
        return dateInscription;
    }

    public void setDateInscription(Timestamp dateInscription) {
        this.dateInscription = dateInscription;
    }

}