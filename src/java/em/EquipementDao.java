/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package em;

import exception.DAOException;
import entity.Equipement;
import entity.Research;
import entity.Utilisateur;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author masla_000
 */
@Stateless

public class EquipementDao {
        // Injection du manager, qui s'occupe de la connexion avec la BDD
    @PersistenceContext( unitName = "SportCenterPU" )
    private EntityManager       em;
    
    public List<Equipement> getAllEquipement(){
        List<Equipement> liste=null;
        Query requete = em.createQuery( "select e from Equipement e" );
        try{
            liste = requete.getResultList();
        } catch ( NoResultException e ) {
            return liste;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return liste;
    }
    
    public void insert(Equipement equipement){
        em.persist(equipement);
    }
    
    public List<Equipement> findAllInColumn(String value, String column){
        List<Equipement> liste=null;
        String requeteString = null;
        switch(column){
            case "All" :
                requeteString = stringFindInAll(value);
                break;
            default : 
                requeteString = "select e from Equipement e where e." + column + " LIKE '%" + value + "%'";
        }
        Query requete = em.createQuery(requeteString );
        try{
            liste = requete.getResultList();
        } catch ( NoResultException e ) {
            return liste;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return liste;
    }
    
    public List<Equipement> findAll(String value){
        List<Equipement> liste=null;
        Query requete = em.createQuery( "select e from Equipement e where " +  stringFindInAll(value));
        try{
            liste = requete.getResultList();
        } catch ( NoResultException e ) {
            return liste;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return liste;
    }
    
    private String stringFindInAll(String value){
        String s =""
                + "e.depCode" + " LIKE '%" + value + "%' OR " 
                + "e.depLib"+ " LIKE '%" + value + "%'" + " OR "
                + "e.commune"+ " LIKE '%" + value + "%'"+ " OR "
                + "e.institution"+ " LIKE '%" + value + "%'"+ " OR "
                + "e.equipementNom"+ " LIKE '%" + value + "%'"+ " OR "
                + "e.equipement"+ " LIKE '%" + value + "%'"+ " OR "
                + "e.anne"+ " LIKE '%" + value + "%'"+ " OR "
                + "e.nature"+ " LIKE '%" + value + "%'"+ " OR "
                + "e.position"+ " LIKE '%" + value + "%'"+ " OR "
                + "e.handi"+ " LIKE '%" + value + "%'";
        return s;
    }
    
    public void registerSearch(Utilisateur user, String value){
        Research search = new Research();
        search.setSearch(value);
        user.addSearch(search);
        search.setUser(user);
        try {
            em.persist( user );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }
    
    
}
