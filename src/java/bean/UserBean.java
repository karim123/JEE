package bean;
 
import em.UtilisateurDao;
import entity.Equipement;
import entity.Utilisateur;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
 
@ManagedBean
@SessionScoped
/**
 *
 * @author User
 */
public class UserBean implements Serializable {
    private Utilisateur user;
    
    @EJB
    private UtilisateurDao    utilisateurDao;

    public UserBean(){
         HttpSession session = Util.getSession();
         user = (Utilisateur) session.getAttribute("user");
    }
    
    public boolean isAdmin(){
        return user.getAdmin();
    }
    
    public void linkAccount(Equipement equipement){
        System.out.println(user.getId());
        user.setEquipementDirector(equipement);
        utilisateurDao.creer( user );

    }
    
    public Utilisateur getUser() {
        HttpSession session = Util.getSession();
        user = (Utilisateur) session.getAttribute("user");
        return user;
    }

    public void setUser(Utilisateur user) {
        this.user = user;
    }
    
}
