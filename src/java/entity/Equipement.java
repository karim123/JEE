/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author masla_000
 */
@Entity
public class Equipement  implements Serializable  {
    @Id
    @GeneratedValue
    private long id;
    private String depCode;
    private String depLib;
    private String commune;
    private String institution;
    private String equipementNom;
    private String equipement;
    private String anne;
    private String nature;
    private String position;
    private String handi;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String depCode) {
        this.depCode = depCode;
    }

    public String getDepLib() {
        return depLib;
    }

    public void setDepLib(String depLib) {
        this.depLib = depLib;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getEquipementNom() {
        return equipementNom;
    }

    public void setEquipementNom(String equipementNom) {
        this.equipementNom = equipementNom;
    }

    public String getEquipement() {
        return equipement;
    }

    public void setEquipement(String equipement) {
        this.equipement = equipement;
    }

    public String getAnne() {
        return anne;
    }

    public void setAnne(String anne) {
        this.anne = anne;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHandi() {
        return handi;
    }

    public void setHandi(String handi) {
        this.handi = handi;
    }
    
    @Override
    public String toString(){
        return " Ville : "+ commune + " Activité : " + equipement;
    }
    
    
}
