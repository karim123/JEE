/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentication;

import bean.Util;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author masla_000
 */
public class AuthenticationFilter implements Filter {
    
        public  void init(FilterConfig filterConfig) {
       // l'objet filterConfig encapsule les paramètres 
       // d'initialisation de ce filtre
       
   }
   
    public  void destroy() {
       // callback de destruction de ce filtre
   }
   
   
    public  void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain)
    throws IOException, ServletException {
        if((((HttpServletRequest)request).getSession().getAttribute("user"))==  null){
            ((HttpServletResponse)response).sendRedirect( "/SportCenter/connexion.xhtml");

        }
         filterChain.doFilter(request, response);
         return;
   }
    
}
