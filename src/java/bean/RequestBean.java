/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import em.RequestDao;
import entity.Request;
import entity.Utilisateur;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;

/**
 *
 * @author masla_000
 */
@ManagedBean
@SessionScoped

public class RequestBean implements Serializable {

    private Request request = new Request();
            @EJB

    private RequestDao dao = new RequestDao();
    
    public String sendRequest(){
        HttpSession session = Util.getSession();
        Utilisateur user =(Utilisateur) session.getAttribute("user");
        if (user != null){
            if(request != null){
            request.setUser(user);
            dao.persist(request);
            }
        }
        return "home?faces-redirect=true;";
    }
    
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
    
    
    
}
