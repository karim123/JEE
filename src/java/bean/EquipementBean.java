/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import em.EquipementDao;
import em.UtilisateurDao;
import entity.Equipement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author masla_000
 */
@ManagedBean
@SessionScoped

public class EquipementBean implements Serializable {
    private String value;
    private Equipement equipement;

    private List<Equipement> liste;
    private List<Equipement> listePagine;


    private int cpt=0;
    private List<String> listeChoix = Arrays.asList("equipement", "commune", "All");
    private String filter;
    
    public String getValue() {
        return value;
    }

    
    
    public Equipement getEquipement() {
        return equipement;
    }

    public void setEquipement(Equipement equipement) {
        this.equipement = equipement;
    }
    public String seeEquipement(Equipement equipement){
        value= "";
        liste=null;
        
        return "equipement?faces-redirect=true;";
    }
    
    public void setValue(String value) {
        this.value = value;
    }

    public List<Equipement> getListe() {
        return liste;
    }

    public void setListe(List<Equipement> liste) {
        this.liste = liste;
    }


    public List<String> getListeChoix() {
        return listeChoix;
    }

    public void setListeChoix(List<String> listeChoix) {
        this.listeChoix = listeChoix;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        
        this.filter = filter;
    }
    
    
    @EJB
    private EquipementDao equipementDao;
    
    public void getAllEquipement(){
        liste =  equipementDao.getAllEquipement();
    }
    

    
    public void getGenerally(){
        liste =  equipementDao.findAllInColumn(value,filter);
    }
    
    public void insertEquipement(Equipement equipement){
        equipementDao.insert(equipement);
    }
}
