package database;
import javax.persistence.*;
import java.util.function.Consumer;


public class DatabaseManager {

    private static EntityManagerFactory SINGLETON_FACTORY;
    private final EntityManager em;

    public DatabaseManager(String applicationMode) {
        this.em = getFactory(applicationMode).createEntityManager();
    }

    public static DatabaseManager getDatabaseManager() {
        String applicationMode = "SportCenterPU";
        return new DatabaseManager(applicationMode);
    }

    private static EntityManagerFactory getFactory(String applicationMode) {
        if (SINGLETON_FACTORY == null) {
            SINGLETON_FACTORY = Persistence.createEntityManagerFactory(applicationMode);
            return SINGLETON_FACTORY;
        }
        return SINGLETON_FACTORY;
    }


    public void save(Object... entities) {
        applyTransaction(em::persist, entities);
    }

    public EntityManager getEntityManager() {
        return em;
    }

    private void applyTransaction(Consumer<Object> consumer, Object... entities) {
        em.getTransaction().begin();
        try {
            for (Object entity : entities) {
                consumer.accept(entity);
            }
            em.getTransaction().commit();
        } catch (PersistenceException e) {
            em.getTransaction().rollback();
        }
    }
}
