package em;

import exception.DAOException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entity.Utilisateur;

@Stateless
public class UtilisateurDao {
    private static final String JPQL_SELECT_PAR_EMAIL = "SELECT u FROM Utilisateur u WHERE u.email=:email";
    private static final String PARAM_EMAIL           = "email";

    // Injection du manager, qui s'occupe de la connexion avec la BDD
    @PersistenceContext( unitName = "SportCenterPU" )
    private EntityManager       em;

    // Enregistrement d'un nouvel utilisateur
    public void creer( Utilisateur utilisateur ) throws DAOException {
        try {
            em.merge( utilisateur );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
    }

    public Utilisateur connexion( String login, String password) throws DAOException{
        Utilisateur utilisateur = null;
        Query requete = em.createQuery("select u from Utilisateur u where u.nom=:login and u.motDePasse=:password");
        requete.setParameter("login", login);
        requete.setParameter("password", password);
        try {
            utilisateur = (Utilisateur) requete.getSingleResult();
        } catch ( NoResultException e ) {
            return utilisateur;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return utilisateur;
    } 
    
    // Recherche d'un utilisateur à partir de son adresse email
    public Utilisateur trouver( String email ) throws DAOException {
        Utilisateur utilisateur = null;
        Query requete = em.createQuery( JPQL_SELECT_PAR_EMAIL );
        requete.setParameter( PARAM_EMAIL, email );
        try {
            utilisateur = (Utilisateur) requete.getSingleResult();
        } catch ( NoResultException e ) {
            return null;
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        return utilisateur;
    }
}