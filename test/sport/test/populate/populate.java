/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sport.test.populate;

import database.DatabaseManager;
import bean.EquipementBean;
import com.opencsv.CSVReader;
import em.EquipementDao;
import entity.Equipement;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author kmaslah
 */
public class populate {


    
    
    @Test
    public void populate() throws NamingException {
        DatabaseManager databaseManager = DatabaseManager.getDatabaseManager();

        try (CSVReader reader = new CSVReader(new FileReader("Equipement.csv"), ';')){
            String[] rowData = null;

            while((rowData = reader.readNext()) != null){
                
                Equipement equipement = new Equipement();
                equipement.setDepCode(rowData[0]);
                equipement.setDepLib(rowData[1]);
                equipement.setCommune(rowData[2]);
                equipement.setInstitution(rowData[3]);
                equipement.setEquipementNom(rowData[4]);
                equipement.setEquipement(rowData[5]);
                equipement.setAnne(rowData[6]);
                equipement.setNature(rowData[7]);
                equipement.setPosition(rowData[8]);
                equipement.setHandi(rowData[9]);
                databaseManager.save(equipement);
            }

        }
        catch (Exception e) {
             e.printStackTrace();

        }

    }
}
