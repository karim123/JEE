/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package em;

import entity.Request;
import exception.DAOException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author masla_000
 */
@Stateless
public class RequestDao {
           
    @PersistenceContext( unitName = "SportCenterPU" )
    private EntityManager       em;
    
    
    public void persist(Request request) throws DAOException{

        try {
            em.persist( request );
        } catch ( Exception e ) {
            throw new DAOException( e );
        }
        
    }
    
    
}
