package entity;

import entity.Utilisateur;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-25T17:31:44")
@StaticMetamodel(Request.class)
public class Request_ { 

    public static volatile SingularAttribute<Request, String> name;
    public static volatile SingularAttribute<Request, String> description;
    public static volatile SingularAttribute<Request, Long> id;
    public static volatile SingularAttribute<Request, Utilisateur> user;

}